/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adread <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/12 19:47:43 by adread            #+#    #+#             */
/*   Updated: 2019/10/20 18:41:48 by adread           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "get_next_line.h"

static t_gnl	*where_is_gnl(int fd)
{
	static t_gnl	*gnl;

	if (!gnl)
	{
		NOTFUNNY((gnl = (t_gnl*)malloc(sizeof(t_gnl))));
		gnl->fd = fd;
		gnl->beyond = NULL;
		gnl->next = NULL;
	}
	while (gnl->next && gnl->fd != fd)
	{
		gnl = gnl->next;
		if (gnl->fd == fd)
			return (gnl);
	}
	if (gnl->fd != fd)
	{
		NOTFUNNY((gnl->next = (t_gnl*)malloc(sizeof(t_gnl))));
		gnl->next->fd = fd;
		gnl->next->beyond = NULL;
		gnl->next->next = NULL;
		gnl = gnl->next;
	}
	NOTFUNNY((gnl->str = ft_strnew(0)));
	return (gnl);
}

static int		heart_gnl(t_gnl *gnl, char **line)
{
	char	*p;

	if (gnl->beyond)
	{
		gnl->str = ft_strjoin(gnl->beyond, gnl->str);
		free(gnl->beyond);
	}
	p = ft_strchr(gnl->str, 10);
	if (p)
	{
		*p = 0;
		*line = ft_strdup(gnl->str);
		if (ft_strlen(gnl->str) + gnl->str > p)
			gnl->beyond = ft_strdup(p + 1);
		free(gnl->str);
		return (1);
	}
	if (!(*gnl->str))
		return (0);
	*line = ft_strdup(gnl->str);
	free(gnl->str);
	return (1);
}

int				get_next_line(const int fd, char **line)
{
	t_gnl			*buf;
	char			*temp;

	if (fd < 0 || line == NULL || BUFF_SIZE < 1 || read(fd, 0, 0) < 0)
		return (-1);
	ERROR((buf = where_is_gnl(fd)));
	buf->tmp = ft_strnew(BUFF_SIZE);
	while ((buf->br = read(fd, buf->tmp, BUFF_SIZE)))
	{
		temp = ft_strjoin(buf->str, buf->tmp);
		free(buf->str);
		buf->str = temp;
		ft_strclr(buf->tmp);
		BRYAK((ft_strchr(buf->str, 10)));
	}
	free(buf->tmp);
	return (heart_gnl(buf, line));
}
