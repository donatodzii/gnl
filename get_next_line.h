/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.h                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adread <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/12 19:50:43 by adread            #+#    #+#             */
/*   Updated: 2019/10/20 18:42:05 by adread           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef GET_NEXT_LINE_H
# define GET_NEXT_LINE_H

# include "./libft/libft.h"
# define BUFF_SIZE 1
# define ERROR(e) if (!e) return (-1);
# define NOTFUNNY(no) if (!no) return (NULL);
# define BRYAK(ch) if (ch) break ;

typedef struct		s_gnl
{
	int				fd;
	int				br;
	char			*str;
	char			*tmp;
	char			*beyond;
	struct s_gnl	*next;
}					t_gnl;

int					get_next_line(const int fd, char **line);

#endif
